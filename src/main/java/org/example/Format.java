package org.example;

import java.util.LinkedHashMap;

public class Format {

    private final StringBuilder result = new StringBuilder();
    public void formatting(LinkedHashMap<Character, Integer> input){
        for (var entry : input.entrySet()) {
            result.append("\"")
                    .append(entry.getKey())
                    .append("\"")
                    .append(" - ")
                    .append(entry.getValue())
                    .append("\n");
        }
        result.deleteCharAt(result.length() - 1);
    }

    public void printResult(){
        System.out.println(result.toString());
    }
    public void clear(){
        result.setLength(0);
    }

    public StringBuilder getResult() {
        return result;
    }
}
