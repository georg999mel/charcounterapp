package org.example;

import java.util.HashMap;
import java.util.LinkedHashMap;

public final class Cache {

    private final HashMap<String, LinkedHashMap<Character, Integer>> cache = new HashMap<>();


    public void addToCache(String key, LinkedHashMap<Character, Integer> value){
        cache.put(key, new LinkedHashMap<>(value));
    }

    public boolean isAlreadyContain(String key){
        return cache.containsKey(key);
    }

    public LinkedHashMap<Character, Integer> getValueByKey(String key) {
        return cache.get(key);
    }
}
