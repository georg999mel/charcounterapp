package org.example;

public enum TextUI {
    START_MASSAGE("Program is running, enter any text for character count, or 'exit' to quit."),
    INPUT_REQUEST_MASSAGE("Enter the string: "),
    ERROR_MASSAGE("You have entered an empty string, please try again."),
    FINISH_MASSAGE("Program finished."),
    FINISH_WORD("exit")
    ;

    private final String value;

    TextUI(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
