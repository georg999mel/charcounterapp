package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

            var cache = new Cache();
            var charsCalculator = new CharsCalculator();
            var format = new Format();
            var scanner = new Scanner(System.in);

            var input = "";

            System.out.println(TextUI.START_MASSAGE.getValue());

            do{
                System.out.println(TextUI.INPUT_REQUEST_MASSAGE.getValue());
                input = scanner.nextLine();

                if(input.equals(TextUI.FINISH_WORD.getValue())){
                    System.out.println(TextUI.FINISH_MASSAGE.getValue());
                    break;
                }

                if(input.isEmpty()){
                    System.out.print(TextUI.ERROR_MASSAGE.getValue());
                    continue;
                }
                if (cache.isAlreadyContain(input)){
                    var result = cache.getValueByKey(input);
                    format.formatting(result);
                } else {
                    charsCalculator.calculateCharsInString(input);
                    var result = charsCalculator.getResult();
                    cache.addToCache(input, result);
                    format.formatting(result);
                }

                format.printResult();
                format.clear();
                charsCalculator.clear();

            } while (true);

    }
}
