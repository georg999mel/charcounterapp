package org.example;

import java.util.LinkedHashMap;

public class CharsCalculator {
    private final LinkedHashMap<Character, Integer> result = new LinkedHashMap<>();

    public void calculateCharsInString(String input){
        for(var i = 0; i < input.length(); i++){
            Character character = input.charAt(i);
            result.merge(character, 1, (value1, value2) -> value1 + 1);
        }
    }

    public LinkedHashMap<Character, Integer> getResult() {
        return result;
    }

    public void clear(){
        result.clear();
    }
}
