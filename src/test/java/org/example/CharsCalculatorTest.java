package org.example;

import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;

class CharsCalculatorTest {

    @Test
    void calculateCharsInStringTestWordWithNumbersCase1(){
        CharsCalculator charsCalculator = new CharsCalculator();
        LinkedHashMap<Character, Integer> expectedResult = new LinkedHashMap<>();
        expectedResult.put('c', 1);
        expectedResult.put('a', 1);   //cattt344
        expectedResult.put('t', 3);
        expectedResult.put('3', 1);
        expectedResult.put('4', 2);
        charsCalculator.calculateCharsInString("cattt344");

        assertEquals(expectedResult, charsCalculator.getResult());
    }

    @Test
    void calculateCharsInStringTestWordWithNumbersCase2(){
        CharsCalculator charsCalculator = new CharsCalculator();
        LinkedHashMap<Character, Integer> expectedResult = new LinkedHashMap<>();
        expectedResult.put('h', 1);
        expectedResult.put('e', 1);  //hello99999990000
        expectedResult.put('l', 2);
        expectedResult.put('o', 1);
        expectedResult.put('9', 7);
        expectedResult.put('0', 4);
        charsCalculator.calculateCharsInString("hello99999990000");

        assertEquals(expectedResult, charsCalculator.getResult());
    }

    @Test
    void calculateCharsInStringTestPhraseCase1(){
        CharsCalculator charsCalculator = new CharsCalculator();
        LinkedHashMap<Character, Integer> expectedResult = new LinkedHashMap<>();
        expectedResult.put('H', 1);
        expectedResult.put('e', 1);   //Hello World!
        expectedResult.put('l', 3);
        expectedResult.put('o', 2);
        expectedResult.put(' ', 1);
        expectedResult.put('W', 1);
        expectedResult.put('r', 1);
        expectedResult.put('d', 1);
        expectedResult.put('!', 1);
        charsCalculator.calculateCharsInString("Hello World!");

        assertEquals(expectedResult, charsCalculator.getResult());
    }

    @Test
    void calculateCharsInStringTestPhraseCase2(){
        CharsCalculator charsCalculator = new CharsCalculator();
        LinkedHashMap<Character, Integer> expectedResult = new LinkedHashMap<>();
        expectedResult.put('H', 1);
        expectedResult.put('i', 3);
        expectedResult.put(',', 1);       //Hi, I'm Georgii.
        expectedResult.put(' ', 2);
        expectedResult.put('I', 1);
        expectedResult.put('\'', 1);
        expectedResult.put('m', 1);
        expectedResult.put('G', 1);
        expectedResult.put('e', 1);
        expectedResult.put('o', 1);
        expectedResult.put('r', 1);
        expectedResult.put('g', 1);
        expectedResult.put('.', 1);
        charsCalculator.calculateCharsInString("Hi, I'm Georgii.");

        assertEquals(expectedResult, charsCalculator.getResult());
    }

    @Test
    void calculateCharsInStringTestRandomSymbolsCase1(){
        CharsCalculator charsCalculator = new CharsCalculator();
        LinkedHashMap<Character, Integer> expectedResult = new LinkedHashMap<>();
        expectedResult.put('*', 1);  //*/!@#5%+ $)(0}?-===
        expectedResult.put('/', 1);
        expectedResult.put('!', 1);
        expectedResult.put('@', 1);
        expectedResult.put('#', 1);
        expectedResult.put('5', 1);
        expectedResult.put('%', 1);
        expectedResult.put('+', 1);
        expectedResult.put(' ', 1);
        expectedResult.put('$', 1);
        expectedResult.put(')', 1);
        expectedResult.put('(', 1);
        expectedResult.put('0', 1);
        expectedResult.put('}', 1);
        expectedResult.put('?', 1);
        expectedResult.put('-', 1);
        expectedResult.put('=', 3);
        charsCalculator.calculateCharsInString("*/!@#5%+ $)(0}?-===");

        assertEquals(expectedResult, charsCalculator.getResult());
    }

}