package org.example;

import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;

class FormatTest {

    @Test
    void formattingTestWordWithNumbersCase1(){
        Format format = new Format();
        LinkedHashMap<Character, Integer> input = new LinkedHashMap<>();
        input.put('c', 1);
        input.put('a', 1);   //cattt344
        input.put('t', 3);
        input.put('3', 1);
        input.put('4', 2);
        format.formatting(input);

        String expectedResult = "\"c\" - 1\n" +
                                "\"a\" - 1\n" +
                                "\"t\" - 3\n" +
                                "\"3\" - 1\n" +
                                "\"4\" - 2";

        assertEquals(expectedResult, format.getResult().toString());
    }

    @Test
    void formattingTestWordWithNumbersCase2(){
        Format format = new Format();
        LinkedHashMap<Character, Integer> input = new LinkedHashMap<>();
        input.put('h', 1);
        input.put('e', 1);  //hello99999990000
        input.put('l', 2);
        input.put('o', 1);
        input.put('9', 7);
        input.put('0', 4);
        format.formatting(input);

        String expectedResult = "\"h\" - 1\n" +
                                "\"e\" - 1\n" +
                                "\"l\" - 2\n" +
                                "\"o\" - 1\n" +
                                "\"9\" - 7\n" +
                                "\"0\" - 4";

        assertEquals(expectedResult, format.getResult().toString());
    }

    @Test
    void formattingTestPhraseCase1(){
        Format format = new Format();
        LinkedHashMap<Character, Integer> input = new LinkedHashMap<>();
        input.put('H', 1);
        input.put('e', 1);   //Hello World!
        input.put('l', 3);
        input.put('o', 2);
        input.put(' ', 1);
        input.put('W', 1);
        input.put('r', 1);
        input.put('d', 1);
        input.put('!', 1);
        format.formatting(input);

        String expectedResult = "\"H\" - 1\n" +
                                "\"e\" - 1\n" +
                                "\"l\" - 3\n" +
                                "\"o\" - 2\n" +
                                "\" \" - 1\n" +
                                "\"W\" - 1\n" +
                                "\"r\" - 1\n" +
                                "\"d\" - 1\n" +
                                "\"!\" - 1";

        assertEquals(expectedResult, format.getResult().toString());
    }

    @Test
    void formattingTestPhraseCase2(){
        Format format = new Format();
        LinkedHashMap<Character, Integer> input = new LinkedHashMap<>();
        input.put('H', 1);
        input.put('i', 3);
        input.put(',', 1);       //Hi, I'm Georgii.
        input.put(' ', 2);
        input.put('I', 1);
        input.put('\'', 1);
        input.put('m', 1);
        input.put('G', 1);
        input.put('e', 1);
        input.put('o', 1);
        input.put('r', 1);
        input.put('g', 1);
        input.put('.', 1);
        format.formatting(input);

        String expectedResult = "\"H\" - 1\n" +
                                "\"i\" - 3\n" +
                                "\",\" - 1\n" +
                                "\" \" - 2\n" +
                                "\"I\" - 1\n" +
                                "\"'\" - 1\n" +
                                "\"m\" - 1\n" +
                                "\"G\" - 1\n" +
                                "\"e\" - 1\n" +
                                "\"o\" - 1\n" +
                                "\"r\" - 1\n" +
                                "\"g\" - 1\n" +
                                "\".\" - 1";

        assertEquals(expectedResult, format.getResult().toString());
    }

    @Test
    void formattingTestRandomSymbolsCase1(){
        Format format = new Format();
        LinkedHashMap<Character, Integer> input = new LinkedHashMap<>();
        input.put('*', 1);  //*/!@#5%+ $)(0}?-===
        input.put('/', 1);
        input.put('!', 1);
        input.put('@', 1);
        input.put('#', 1);
        input.put('5', 1);
        input.put('%', 1);
        input.put('+', 1);
        input.put(' ', 1);
        input.put('$', 1);
        input.put(')', 1);
        input.put('(', 1);
        input.put('0', 1);
        input.put('}', 1);
        input.put('?', 1);
        input.put('-', 1);
        input.put('=', 3);
        format.formatting(input);

        String expectedResult = "\"*\" - 1\n" +
                                "\"/\" - 1\n" +
                                "\"!\" - 1\n" +
                                "\"@\" - 1\n" +
                                "\"#\" - 1\n" +
                                "\"5\" - 1\n" +
                                "\"%\" - 1\n" +
                                "\"+\" - 1\n" +
                                "\" \" - 1\n" +
                                "\"$\" - 1\n" +
                                "\")\" - 1\n" +
                                "\"(\" - 1\n" +
                                "\"0\" - 1\n" +
                                "\"}\" - 1\n" +
                                "\"?\" - 1\n" +
                                "\"-\" - 1\n" +
                                "\"=\" - 3";

        assertEquals(expectedResult, format.getResult().toString());
    }

}